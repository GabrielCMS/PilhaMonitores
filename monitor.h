#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define tam 15

typedef struct Monitor{
  char resolucao[10];
  char modelo[10];
  float preco;
}Monitor;

typedef struct Pilha{
  Monitor itens[tam];
  int topo;
}Pilha;

void iniciarPilha(Pilha *p){
  p->topo = -1;
};

void push(Pilha *p, char resolucao[], char modelo[], float preco){
  Monitor newMonitor;
  strcpy(newMonitor.resolucao, resolucao);
  strcpy(newMonitor.modelo, modelo);
  newMonitor.preco = preco;
  (*p).topo = p->topo+1;
  (*p).itens[p->topo] = newMonitor;
  printf("Monitor Cadastrado!\n");
  printf("\n");
}

void pop(Pilha *p){

  printf("Ultimo Monitor cadastrado foi removido! \n");
  printf("\n");
  (*p).topo--;
}

void mostrarTopo(Pilha *p){
  printf("Ultimo Monitor Cadastrado\n");
    printf("resolucao do Monitor: %s\n", p->itens[p->topo].resolucao);
    printf("modelo do Monitor: %s\n", p->itens[p->topo].modelo);
    printf("Preco do Monitor: %f\n", p->itens[p->topo].preco);
    printf("\n");
}
